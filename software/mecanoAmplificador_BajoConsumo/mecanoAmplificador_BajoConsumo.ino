#include "LowPower.h"

//Proyecto Amplificacion Mecanoceptiva
//Dispositivo Mecano-Amplificador
//Autores:
//        Maite Aliaga
//        Manuel Hidalgo
//Fecha: Junio 2017
//Fecha: Abril 2018. Modificacion del ciclo de trabajo 25%
//Código con licencia Creative Commons (by-nc-sa)

//declaracion de variables y pines
int sensorPinA2 = A2;   // Sensor conectado a Analog 2
int motorPinD3 = 3;     // LED conectado a Pin 3 (PWM)
int resRead;            // La Lectura de la Resistencia por División de Tensión
int umbral;             // Umbral de activacion
int potPinA0 = A0;      // Marca el umbral conectado a Analog 0

//programa

void Wakeup(){

 //  Serial.println("Interrupt");
 //  Serial.flush();
}

void setup()
{
  Serial.begin(9600); // Enviaremos la información de depuración a través del Monitor de Serial
  pinMode(motorPinD3, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(4,INPUT);
  pinMode(A3, INPUT);
  digitalWrite(LED_BUILTIN, LOW);



}
//programa principal



void loop()
{
  resRead = analogRead(sensorPinA2); // lectura del sensor (A2)
  //escribe el valor de la lectura del sensor de fuerza
  Serial.print("Lectura sensor de fuerza = ");
  Serial.print(resRead);

  umbral = analogRead(potPinA0); // umbral (A0) es igual a la lectura del sensor (A2)
  //escribe el valor de la lectura del umbral
  Serial.print("\tLectura del umbral = ");
  Serial.println(umbral);

  //activa el motor-vibrador si cumple la condicion
  //el ciclo de trabajo del motor-vibrador 25%
  if (resRead > umbral) {
    digitalWrite(motorPinD3, 1);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(750);
  }

  //desactiva el motor-vibrador
  digitalWrite(motorPinD3, 0);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);

  for(int i=0;i<=7000;i++){
        if(analogRead(sensorPinA2)>10){
          i=0;
          break;
        }
        if(i==6998){
        attachInterrupt( 0,Wakeup, RISING);
        LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
        detachInterrupt(0);
        }
        
  }

  
}
